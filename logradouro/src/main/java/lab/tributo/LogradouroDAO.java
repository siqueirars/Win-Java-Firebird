package lab.tributo;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import java.util.List;
import org.demoiselle.jee.crud.AbstractDAO;

public class LogradouroDAO extends AbstractDAO<Logradouro, Long>  {

	@PersistenceContext
	protected EntityManager em;
	
	@Override
	protected EntityManager getEntityManager () {
		return em;
	}

	@SuppressWarnings("unchecked")
	public List<Logradouro> listarLogradouros() throws Exception {
		
		List<Logradouro> logradouros = null;
		 try {
		  logradouros = em.createNamedQuery("Logradouro.findAll").getResultList();
		 } catch (Exception e) {
		  e.printStackTrace();
		 }
		 return logradouros;
    }

	public Logradouro find(Integer cod) {
		
		if (cod == null) {
			return null;
		}

		Logradouro logradouro = null;

		try {
		    logradouro = em.createNamedQuery("Logradouro.find", Logradouro.class)
		    		.setParameter("cd", cod)
		    		.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	    return logradouro;	    
	}    

	public Logradouro findOutro(Integer cod) {
	    return this.em
	        .createNamedQuery("Logradouro.find", Logradouro.class)
	        .setParameter("cd", cod)
	        .getSingleResult();
	}
}
