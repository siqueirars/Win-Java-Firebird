package lab.tributo;

import javax.persistence.*;


/**
 * The persistent class for the BAIRROS database table.
 * 
 */
@Entity
@Table(name="BAIRROS")
@NamedQueries({
	@NamedQuery(name="Bairro.findAll", query="SELECT b FROM Bairro b"),
	@NamedQuery(name="Bairro.find", query="SELECT b FROM Bairro b where b.baiCd = :cd ")
})	
@SequenceGenerator(name = "SQ_BAI_CD", sequenceName = "sq_bai_cd", allocationSize = 1)
public class Bairro {
//public class Bairro implements Serializable {
//	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_BAI_CD")
	@Column(name="BAI_CD")
	private int baiCd;

	@Column(name="BAI_NM")
	private String baiNm;

	public Bairro() {
	}

	public int getBaiCd() {
		return this.baiCd;
	}

	public void setBaiCd(int baiCd) {
		this.baiCd = baiCd;
	}

	public String getBaiNm() {
		return this.baiNm;
	}

	public void setBaiNm(String baiNm) {
		this.baiNm = baiNm;
	}

}