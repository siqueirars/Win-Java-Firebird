package lab.tributo;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import lab.tributo.LogradouroDAO;

import io.swagger.annotations.ApiOperation;

//@Api("Bairros")
@Path("")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@io.swagger.annotations.Api(value = "Logradouro")

public class LogradouroREST {

	@Inject
	protected LogradouroDAO dao;

    @GET
	@Path("logradouros")
	@ApiOperation(value = "(RBAC) Recupera ......", notes = "Fornece lista de ...", 
	response = Response.class)
	public Response listarLogradouros(@Context HttpServletRequest httpServletRequest, @Context UriInfo uriInfo)
			throws Exception {
    	List<Logradouro> logradouros = dao.listarLogradouros();
		return Response.ok().entity(logradouros).build();
    }
    

	@GET
    @Path("/logradouro/{logCd}")
	@ApiOperation(value = "(RBAC) Recupera ......", notes = "Fornece lista de ...", 
	response = Response.class)
    public Logradouro getLogradouroById(@PathParam("logCd") Integer logCd){
        Logradouro logradouro = dao.find(logCd);
//        bairro.setBaiCd(baiId);
//        bairro.setBaiNm("MARCO");
        return logradouro;
	}    

}
