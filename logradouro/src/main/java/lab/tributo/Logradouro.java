package lab.tributo;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the LOGRADOUROS database table.
 * 
 */
@Entity
@Table(name="LOGRADOUROS")
@NamedQueries({
	@NamedQuery(name="Logradouro.findAll", query="SELECT l FROM Logradouro l "),
	@NamedQuery(name="Logradouro.find", query="SELECT l FROM Logradouro l where l.logCd = :cd  ")
}) 
@SequenceGenerator(name = "SQ_LOG_CD", sequenceName = "sq_log_cd", allocationSize = 1)
public class Logradouro {
//public class Logradouro implements Serializable {
//	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_LOG_CD")
	@Column(name="LOG_CD")
	private int logCd;

	@Column(name="LOG_NM")
	private String logNm;

	@Column(name="LOG_TP")
	private String logTp;

	public Logradouro() {
	}

	public int getLogCd() {
		return this.logCd;
	}

	public void setLogCd(int logCd) {
		this.logCd = logCd;
	}

	public String getLogNm() {
		return this.logNm;
	}

	public void setLogNm(String logNm) {
		this.logNm = logNm;
	}

	public String getLogTp() {
		return this.logTp;
	}

	public void setLogTp(String logTp) {
		this.logTp = logTp;
	}

}