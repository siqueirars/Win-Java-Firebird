package lab.tributo;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import lab.tributo.BairroDAO;

import io.swagger.annotations.ApiOperation;

//@Api("Bairros")
@Path("")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@io.swagger.annotations.Api(value = "Bairros")

public class BairroREST {

	@Inject
	protected BairroDAO dao;

    @GET
	@Path("bairros")
	@ApiOperation(value = "(RBAC) Recupera ......", notes = "Fornece lista de ...", 
	response = Response.class)
	public Response listarBairros(@Context HttpServletRequest httpServletRequest, @Context UriInfo uriInfo)
			throws Exception {
    	List<Bairro> bairros = dao.listarBairros();
		return Response.ok().entity(bairros).build();
    }
    

    @GET
	@Path("bairros2")
	@ApiOperation(value = "(RBAC) Recupera ......", notes = "Fornece lista de ...", 
	response = Response.class)
	public Response listarBairros2(@Context HttpServletRequest httpServletRequest, @Context UriInfo uriInfo)
			throws Exception {
    	List<Bairro> bairros = dao.listarBairros2();
		return Response.ok().entity(bairros).build();
    }
    
	@GET
    @Path("/bairro/{baiId}")
	@ApiOperation(value = "(RBAC) Recupera ......", notes = "Fornece lista de ...", 
	response = Response.class)
    public Bairro getBairroById(@PathParam("baiId") Integer baiId){
        Bairro bairro = dao.find(baiId);
//        bairro.setBaiCd(baiId);
//        bairro.setBaiNm("MARCO");
        return bairro;
	}    

    @GET
    @Path("/hello")
    public Client getCliente() {
        Client client = new Client();
        client.setName("Hello World ALL");
        return client;
    }

/*    
    @Path("/bairro/{id}")
    @GET
    @Produces( { MediaType.APPLICATION_XML })
    public Bairro getBaiById(@PathParam("id") Long id) {
       BairroDAO bairroDAO = new BairroDAO();
       Bairro bairro = bairroDAO.getBaiById(id);
       return bairro;
    }
*/    
}
