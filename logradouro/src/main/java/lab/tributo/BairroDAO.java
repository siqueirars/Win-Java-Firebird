package lab.tributo;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import java.util.List;
import org.demoiselle.jee.crud.AbstractDAO;

public class BairroDAO extends AbstractDAO<Bairro, Long>  {

	@PersistenceContext
	protected EntityManager em;
	
	@Override
	protected EntityManager getEntityManager () {
		return em;
	}

	@SuppressWarnings("unchecked")
	public List<Bairro> listarBairros() throws Exception {
		
		List<Bairro> bairros = null;
		 try {
		  bairros = em.createNamedQuery("Bairro.findAll").getResultList();
		 } catch (Exception e) {
		  e.printStackTrace();
		 }
		 return bairros;
    }

	
	public List<Bairro> listarBairros2() {

		return getEntityManager().createQuery("select b from " + Bairro.class.getSimpleName() + " b", Bairro.class).getResultList();

	}

	public Bairro find(Integer cod) {
		
		if (cod == null) {
			return null;
		}
		
//	    String sql = " select b from Bairro b where b.baiCd = :cd ";
	    
	    Bairro bairro = null;
	    try {
//		    Query query = em.createQuery(sql, Bairro.class);
		    bairro = em.createNamedQuery("Bairro.find", Bairro.class)
		    		.setParameter("cd", cod)
		    		.getSingleResult();
//		    query.setParameter("cd", cod);	    	   	    	
//	    	bairro = (Bairro)query.getSingleResult();			
		} catch (Exception e) {
			return null;
		}
	    return bairro;	    
	}    

	public Bairro findOutro(Integer cod) {
	    return this.em
	        .createNamedQuery("Bairro.find", Bairro.class)
	        .setParameter("cd", cod)
	        .getSingleResult();
	}
}
