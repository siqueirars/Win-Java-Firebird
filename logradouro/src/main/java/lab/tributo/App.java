package lab.tributo;

import java.util.ResourceBundle;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import io.swagger.jaxrs.config.BeanConfig;

@ApplicationPath("api")
public class App extends Application{
	public App() {
		ResourceBundle bundle = ResourceBundle.getBundle("swagger");

		BeanConfig beanConfig = new BeanConfig();

		beanConfig.setVersion(bundle.getString("swagger.doc.version"));
		beanConfig.setBasePath(bundle.getString("swagger.base.path"));
		beanConfig.setResourcePackage(bundle.getString("swagger.root.package"));
		beanConfig.setTitle(bundle.getString("swagger.api.title"));
		beanConfig.setDescription(bundle.getString("swagger.api.description"));
		beanConfig.setContact(bundle.getString("swagger.api.contact"));
		//beanConfig.setSchemes(new String[] { "HTTPS" });
		beanConfig.setScan(true);
	}
}

